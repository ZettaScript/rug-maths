macro_rules! impl_arith {
	(
		$Struct:ident,
		$Imp:ident { $method:ident },
		$ImpAssign:ident { $method_assign:ident }
	) => {
		impl<B> $Imp<B> for $Struct
		where
			rug::$Struct: ops::$Imp<B, Output = rug::$Struct>,
		{
			type Output = $Struct;

			fn $method(self, rhs: B) -> Self::Output {
				$Struct {
					val: ops::$Imp::$method(self.val, rhs),
				}
			}
		}

		impl<'a, B> $Imp<B> for &'a $Struct
		where
			rug::$Struct: ops::$Imp<B, Output = rug::$Struct>,
		{
			type Output = $Struct;

			fn $method(self, rhs: B) -> Self::Output {
				$Struct {
					val: ops::$Imp::$method(self.val.clone(), rhs),
				}
			}
		}

		impl $Imp<$Struct> for $Struct {
			type Output = $Struct;

			fn $method(self, rhs: $Struct) -> Self::Output {
				$Struct {
					val: ops::$Imp::$method(self.val, rhs.val),
				}
			}
		}

		impl<'a> $Imp<&'a $Struct> for $Struct {
			type Output = $Struct;

			fn $method(self, rhs: &Self) -> Self::Output {
				$Struct {
					val: ops::$Imp::$method(self.val, &rhs.val),
				}
			}
		}

		impl<'a> $Imp<$Struct> for &'a $Struct {
			type Output = $Struct;

			fn $method(self, rhs: $Struct) -> Self::Output {
				$Struct {
					val: ops::$Imp::$method(self.val.clone(), rhs.val),
				}
			}
		}

		impl<'a> $Imp<&'a $Struct> for &'a $Struct {
			type Output = $Struct;

			fn $method(self, rhs: &'a $Struct) -> Self::Output {
				$Struct {
					val: ops::$Imp::$method(self.val.clone(), &rhs.val),
				}
			}
		}

		impl<B> $ImpAssign<B> for $Struct
		where
			rug::$Struct: $ImpAssign<B>,
		{
			fn $method_assign(&mut self, rhs: B) {
				self.val.$method_assign(rhs);
			}
		}

		impl $ImpAssign<$Struct> for $Struct {
			fn $method_assign(&mut self, rhs: Self) {
				self.val.$method_assign(rhs.val);
			}
		}

		impl<'a> $ImpAssign<&'a $Struct> for $Struct {
			fn $method_assign(&mut self, rhs: &Self) {
				self.val.$method_assign(&rhs.val);
			}
		}
	};
}

#[allow(unused_macros)]
macro_rules! impl_arith_diff {
	(
		$Struct:ident,
		$Imp:ident { $method:ident },
		$ImpAssign:ident { $method_assign:ident }
	) => {
		impl<B> $Imp<B> for $Struct
		where
			rug::$Struct: ops::$Imp<B, Output = rug::$Struct>,
		{
			type Output = $Struct;

			fn $method(self, rhs: B) -> Self::Output {
				$Struct {
					val: ops::$Imp::$method(self.val, rhs),
				}
			}
		}

		impl<'a, B> $Imp<B> for &'a $Struct
		where
			rug::$Struct: ops::$Imp<B, Output = rug::$Struct>,
		{
			type Output = $Struct;

			fn $method(self, rhs: B) -> Self::Output {
				$Struct {
					val: ops::$Imp::$method(self.val.clone(), rhs),
				}
			}
		}

		impl<B> $ImpAssign<B> for $Struct
		where
			rug::$Struct: $ImpAssign<B>,
		{
			fn $method_assign(&mut self, rhs: B) {
				self.val.$method_assign(rhs);
			}
		}
	};
}
