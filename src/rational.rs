extern crate maths_traits;
extern crate num_traits;
extern crate rug;

#[path = "macros.rs"]
#[macro_use]
mod macros;

use maths_traits::{algebra, algebra::additive::*, algebra::multiplicative::*, analysis};
use std::fmt::{Display, Formatter, Result as FmtResult};
use std::ops;

#[derive(Clone, Debug, PartialEq, PartialOrd, Eq, Ord)]
pub struct Rational {
	pub val: rug::Rational,
}

impl Display for Rational {
	fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
		self.val.fmt(f)
	}
}

impl<T> From<T> for Rational
where
	rug::Rational: From<T>,
{
	fn from(src: T) -> Self {
		Rational {
			val: rug::Rational::from(src),
		}
	}
}

impl AddAssociative for Rational {}
impl AddCommutative for Rational {}
impl MulAssociative for Rational {}
impl MulCommutative for Rational {}

impl analysis::ordered::AddOrdered for Rational {}
impl analysis::ordered::MulOrdered for Rational {}

impl analysis::ordered::ArchimedeanProperty for Rational {}

impl_arith! { Rational, Add { add }, AddAssign {add_assign} }
impl_arith! { Rational, Sub { sub }, SubAssign {sub_assign} }
impl_arith! { Rational, Mul { mul }, MulAssign {mul_assign} }
impl_arith! { Rational, Div { div }, DivAssign {div_assign} }

impl algebra::group_like::additive::Neg for Rational {
	type Output = Rational;

	fn neg(self) -> Self::Output {
		Self::Output { val: -self.val }
	}
}

impl algebra::group_like::additive::Zero for Rational {
	fn zero() -> Self {
		Self {
			val: rug::Rational::new(),
		}
	}

	fn is_zero(&self) -> bool {
		self.val == 0
	}

	#[allow(unused_must_use)]
	fn set_zero(&mut self) {
		self.val.assign_f32(0.0);
	}
}

impl algebra::group_like::multiplicative::One for Rational {
	fn one() -> Self {
		Self {
			val: rug::Rational::from(1),
		}
	}

	fn is_one(&self) -> bool {
		self.val == 1
	}

	#[allow(unused_must_use)]
	fn set_one(&mut self) {
		self.val.assign_f32(1.0);
	}
}

impl algebra::group_like::multiplicative::Inv for Rational {
	type Output = Rational;

	fn inv(self) -> Self::Output {
		Self::Output {
			val: self.val.recip(),
		}
	}
}

impl algebra::ring_like::Distributive for Rational {}

impl algebra::ring_like::Divisibility for Rational {
	fn divides(self, rhs: Self) -> bool {
		let q = rhs / self;
		q.val == q.val.clone().trunc()
	}

	fn divide(self, rhs: Self) -> Option<Self> {
		let q = rhs / self;
		if q.val == q.val.clone().trunc() {
			Some(q)
		} else {
			None
		}
	}

	fn unit(&self) -> bool {
		!self.is_zero()
	}

	fn inverse(self) -> Option<Self> {
		match self.unit() {
			false => None,
			true => Some(self.inv()),
		}
	}
}

impl analysis::ordered::Sign for Rational {
	fn signum(self) -> Self {
		Self {
			val: self.val.signum(),
		}
	}

	fn abs(self) -> Self {
		Self {
			val: self.val.abs(),
		}
	}
}

impl algebra::ring_like::NoZeroDivisors for Rational {}
impl algebra::ring_like::UniquelyFactorizable for Rational {}

#[cfg(test)]
mod tests {
	use super::*;

	use algebra::ring_like::Divisibility;

	#[test]
	fn test_integer_ops() {
		let a = Rational::from(16);
		let b = Rational::from(10);
		let c = Rational::from(-6);

		assert_eq!(b.clone() - a.clone(), c);
		assert_eq!(&b - a.clone(), c);
		assert_eq!(b.clone() - &a, c);
		assert_eq!(&b - &a, c);
	}

	#[test]
	fn test_rational_ops() {
		let a = Rational::from((19, 7));
		let b = Rational::from((2, 7));
		let c = Rational::from(3);

		assert_eq!(b.clone() + a.clone(), c);
		assert_eq!(&b + a.clone(), c);
		assert_eq!(b.clone() + &a, c);
		assert_eq!(&b + &a, c);
		assert!((&a - &b * &c * 2).is_one());
	}

	#[test]
	fn test_inv() {
		assert_eq!(Rational::from((2, 3)).inv(), Rational::from((3, 2)));
	}

	#[test]
	fn test_divisibility() {
		assert!(Rational::from((5, 7)).divides(Rational::from((15, 7))));
		assert!(!Rational::from((5, 7)).divides(Rational::from((16, 7))));
	}
}
