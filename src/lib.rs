pub(crate) mod complex;
pub(crate) mod float;
pub(crate) mod integer;
pub(crate) mod rational;

pub use complex::Complex;
pub use float::Float;
pub use integer::Integer;
pub use rational::Rational;

use maths_traits::{analysis, analysis::Sign};
use num_traits::{ToPrimitive, Zero};

impl Float {
	pub fn to_rational(&self) -> Option<Rational> {
		Some(Rational {
			val: self.val.to_rational()?,
		})
	}
}

impl Complex {
	/// Returns the real part
	pub fn real(&self) -> Float {
		Float {
			val: self.val.real().clone(),
		}
	}

	/// Returns the imaginary part
	pub fn imag(&self) -> Float {
		Float {
			val: self.val.imag().clone(),
		}
	}
}

impl analysis::real::ComplexSubset for Float {
	type Real = Float;
	type Natural = Integer;
	type Integer = Integer;

	fn as_real(self) -> Self::Real {
		self
	}

	fn as_natural(self) -> Self::Natural {
		Self::Natural {
			val: self.val.to_integer().unwrap().abs(),
		}
	}

	fn as_integer(self) -> Self::Integer {
		Self::Natural {
			val: self.val.to_integer().unwrap(),
		}
	}

	fn floor(self) -> Self {
		Self {
			val: self.val.floor(),
		}
	}

	fn ceil(self) -> Self {
		Self {
			val: self.val.ceil(),
		}
	}

	fn round(self) -> Self {
		Self {
			val: self.val.round(),
		}
	}

	fn trunc(self) -> Self {
		Self {
			val: self.val.trunc(),
		}
	}

	fn fract(self) -> Self {
		Self {
			val: self.val.fract(),
		}
	}

	fn im(self) -> Self {
		Self::zero()
	}

	fn re(self) -> Self {
		self
	}

	fn conj(self) -> Self {
		self
	}
}

impl analysis::real::ComplexSubset for Integer {
	type Real = Float;
	type Natural = Integer;
	type Integer = Integer;

	fn as_real(self) -> Self::Real {
		// ugly!
		Float::from(self.to_i128().unwrap())
	}

	fn as_natural(self) -> Self::Natural {
		self.abs()
	}

	fn as_integer(self) -> Self::Integer {
		self
	}

	fn floor(self) -> Self {
		self
	}

	fn ceil(self) -> Self {
		self
	}

	fn round(self) -> Self {
		self
	}

	fn trunc(self) -> Self {
		self
	}

	fn fract(self) -> Self {
		Self::zero()
	}

	fn im(self) -> Self {
		Self::zero()
	}

	fn re(self) -> Self {
		self
	}

	fn conj(self) -> Self {
		self
	}
}

impl analysis::real::ComplexSubset for Rational {
	type Real = Float;
	type Natural = Integer;
	type Integer = Integer;

	fn as_real(self) -> Self::Real {
		Self::Real::from(self.val.to_f64())
	}

	fn as_natural(self) -> Self::Natural {
		self.as_real().as_natural()
	}

	fn as_integer(self) -> Self::Integer {
		self.as_real().as_integer()
	}

	fn floor(self) -> Self {
		Self {
			val: self.val.floor(),
		}
	}

	fn ceil(self) -> Self {
		Self {
			val: self.val.ceil(),
		}
	}

	fn round(self) -> Self {
		Self {
			val: self.val.round(),
		}
	}

	fn trunc(self) -> Self {
		Self {
			val: self.val.trunc(),
		}
	}

	fn fract(self) -> Self {
		Self {
			val: self.val.fract_trunc(rug::Integer::new()).0,
		}
	}

	fn im(self) -> Self {
		Self::zero()
	}

	fn re(self) -> Self {
		self
	}

	fn conj(self) -> Self {
		self
	}
}

impl analysis::real::ComplexSubset for Complex {
	type Real = Float;
	type Natural = Integer;
	type Integer = Integer;

	fn as_real(self) -> Self::Real {
		self.real()
	}

	fn as_natural(self) -> Self::Natural {
		self.real().as_natural()
	}

	fn as_integer(self) -> Self::Integer {
		self.real().as_integer()
	}

	fn floor(self) -> Self {
		Self {
			val: rug::Complex::with_val(self.val.prec(), {
				let (real, imag) = self.val.into_real_imag();
				(real.floor(), imag.floor())
			}),
		}
	}

	fn ceil(self) -> Self {
		Self {
			val: rug::Complex::with_val(self.val.prec(), {
				let (real, imag) = self.val.into_real_imag();
				(real.ceil(), imag.ceil())
			}),
		}
	}

	fn round(self) -> Self {
		Self {
			val: rug::Complex::with_val(self.val.prec(), {
				let (real, imag) = self.val.into_real_imag();
				(real.round(), imag.round())
			}),
		}
	}

	fn trunc(self) -> Self {
		Self {
			val: rug::Complex::with_val(self.val.prec(), {
				let (real, imag) = self.val.into_real_imag();
				(real.trunc(), imag.trunc())
			}),
		}
	}

	fn fract(self) -> Self {
		Self {
			val: rug::Complex::with_val(self.val.prec(), {
				let (real, imag) = self.val.into_real_imag();
				(real.fract(), imag.fract())
			}),
		}
	}

	fn im(self) -> Self {
		Self {
			val: rug::Complex::with_val(self.val.prec(), (0, self.val.into_real_imag().1)),
		}
	}

	fn re(self) -> Self {
		Self {
			val: rug::Complex::with_val(self.val.prec(), (self.val.into_real_imag().0, 0)),
		}
	}

	fn conj(self) -> Self {
		Self {
			val: self.val.conj(),
		}
	}
}
