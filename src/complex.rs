extern crate maths_traits;
extern crate num_traits;
extern crate rug;

#[path = "macros.rs"]
#[macro_use]
mod macros;

use maths_traits::{
	algebra,
	algebra::{additive::*, multiplicative::*},
	analysis,
};
use num_traits::ops::checked::*;
use std::{
	fmt::{Display, Formatter, Result as FmtResult},
	ops,
};

const COMPLEX_DEFAULT_PREC: (u32, u32) = (32, 32);

#[derive(Clone, Debug, PartialEq)]
pub struct Complex {
	pub val: rug::Complex,
}

impl Complex {
	pub fn with_val<P, T>(prec: P, val: T) -> Self
	where
		rug::Complex: rug::Assign<T>,
		P: rug::complex::Prec,
	{
		Complex {
			val: rug::Complex::with_val(prec, val),
		}
	}
}

impl Display for Complex {
	fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
		self.val.fmt(f)
	}
}

impl<T> From<T> for Complex
where
	rug::Complex: From<T>,
{
	fn from(src: T) -> Self {
		Complex {
			val: rug::Complex::from(src),
		}
	}
}

impl AddAssociative for Complex {}
impl AddCommutative for Complex {}
impl MulAssociative for Complex {}
impl MulCommutative for Complex {}

impl_arith! { Complex, Add { add }, AddAssign {add_assign} }
impl_arith! { Complex, Sub { sub }, SubAssign {sub_assign} }
impl_arith! { Complex, Mul { mul }, MulAssign {mul_assign} }
impl_arith! { Complex, Div { div }, DivAssign {div_assign} }

impl CheckedAdd for Complex {
	fn checked_add(&self, v: &Self) -> Option<Self> {
		Some(self + v)
	}
}

impl CheckedSub for Complex {
	fn checked_sub(&self, v: &Self) -> Option<Self> {
		Some(self - v)
	}
}

impl CheckedNeg for Complex {
	fn checked_neg(&self) -> Option<Self> {
		Some(Self {
			val: -self.val.clone(),
		})
	}
}

impl CheckedMul for Complex {
	fn checked_mul(&self, v: &Self) -> Option<Self> {
		Some(self * v)
	}
}

impl CheckedDiv for Complex {
	fn checked_div(&self, v: &Self) -> Option<Self> {
		if v.is_zero() {
			None
		} else {
			Some(self / v)
		}
	}
}

impl algebra::group_like::additive::Neg for Complex {
	type Output = Complex;

	fn neg(self) -> Self::Output {
		Complex { val: -self.val }
	}
}

impl algebra::group_like::additive::Zero for Complex {
	fn zero() -> Self {
		Complex {
			val: rug::Complex::new(COMPLEX_DEFAULT_PREC),
		}
	}

	fn is_zero(&self) -> bool {
		self.val == 0
	}

	fn set_zero(&mut self) {
		self.val *= 0;
	}
}

impl algebra::group_like::multiplicative::One for Complex {
	fn one() -> Self {
		Complex {
			val: rug::Complex::with_val(COMPLEX_DEFAULT_PREC, 1),
		}
	}

	fn is_one(&self) -> bool {
		self.val == 1
	}

	fn set_one(&mut self) {
		self.val *= 0;
		self.val += 1;
	}
}

impl algebra::group_like::multiplicative::Inv for Complex {
	type Output = Complex;

	fn inv(self) -> Self::Output {
		Complex { val: 1 / self.val }
	}
}

impl algebra::ring_like::Distributive for Complex {}

impl algebra::ring_like::Divisibility for Complex {
	fn divides(self, rhs: Self) -> bool {
		let q = rhs / self;
		q.val.real() == &q.val.real().clone().trunc()
			&& q.val.imag() == &q.val.imag().clone().trunc()
	}

	fn divide(self, rhs: Self) -> Option<Self> {
		match self.clone().divides(rhs.clone()) {
			false => None,
			true => Some(rhs / self),
		}
	}

	fn unit(&self) -> bool {
		!self.is_zero()
	}

	fn inverse(self) -> Option<Self> {
		match self.unit() {
			false => None,
			true => Some(self.inv()),
		}
	}
}

impl algebra::ring_like::NoZeroDivisors for Complex {}

impl algebra::ring_like::Exponential for Complex {
	fn exp(self) -> Self {
		Complex {
			val: self.val.exp(),
		}
	}

	fn try_ln(self) -> Option<Self> {
		Some(Complex { val: self.val.ln() })
	}
}

impl analysis::real::RealExponential for Complex {}

macro_rules! complex_fn_trig {
	($fn:ident) => {
		fn $fn(self) -> Self {
			Complex {
				val: self.val.$fn(),
			}
		}
	};
}

macro_rules! complex_fn_trig_try {
	($fn:ident $try_fn:ident) => {
		fn $try_fn(self) -> Option<Self> {
			let result = self.val.$fn();
			if result.real().is_nan() || result.imag().is_nan() {
				return None;
			}
			Some(Complex { val: result })
		}
	};
}

impl analysis::real::Trig for Complex {
	complex_fn_trig! {sin}
	complex_fn_trig! {cos}
	complex_fn_trig! {tan}
	complex_fn_trig! {sinh}
	complex_fn_trig! {cosh}
	complex_fn_trig! {tanh}
	complex_fn_trig! {asin}
	complex_fn_trig! {acos}
	complex_fn_trig! {atan}
	complex_fn_trig! {asinh}
	complex_fn_trig! {acosh}
	complex_fn_trig! {atanh}
	complex_fn_trig_try! {asin try_asin}
	complex_fn_trig_try! {acos try_acos}
	complex_fn_trig_try! {asinh try_asinh}
	complex_fn_trig_try! {acosh try_acosh}
	complex_fn_trig_try! {atanh try_atanh}

	fn atan2(_y: Self, _x: Self) -> Self {
		unimplemented!("This looks meaningless");
	}

	fn pi() -> Self {
		Complex {
			val: rug::Complex::with_val(COMPLEX_DEFAULT_PREC, rug::float::Constant::Pi),
		}
	}
}
