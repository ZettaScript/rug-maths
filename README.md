# rug-maths

A wrapper for the [rug](https://gitlab.com/tspiteri/rug) structs, implementing [maths-traits](https://github.com/anvil777/maths-traits) traits. It enables computing with arbitrary precision, with algebra & analysis traits.

## Example

	let z = Complex::with_val(53, (123, 369)); // 123 + 369i
	let a = z.real(); // 123
	let b = z.imag(); // 369
	
	let n = Integer::from(101);
	assert!(n.prime()); // Check primality
	assert!(!(n+1).prime());
	
	let a = Integer::from(27);
	let b = Integer::from(72);
	assert_eq!(a.clone().gcd(b.clone()), Integer::from(9)); // Compute GCD
	assert_eq!(a.lcm(b), Integer::from(216)); // Compute LCM
